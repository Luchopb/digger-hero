﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class TreasureBehaviour : MonoBehaviour
{
    private SpriteRenderer _mySprite;
    private bool _canTouch;
    private DigSiteType _myDigSite;

    private void Awake()
    {
        _mySprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        _mySprite.color = Color.black;
    }

    public void Reach(DigSiteType digsite)
    {
        _mySprite.DOColor(Color.white, 0.5f);
        _canTouch = true;
        _myDigSite = digsite;
    }

    public void ChangeDigSite(bool reached, DigSiteType digsite)
    {
        _canTouch = reached;
        _myDigSite = digsite;
        _mySprite.color = _canTouch ? Color.white : Color.black;
    }
    
    private void OnMouseDown()
    {
        if (!_canTouch) return;
        
        GenerateTreasure();
    }

    public void GenerateTreasure()
    {
        var totalChance = 0f;
        //Acumulo las chances
        for (int i = 0; i < _myDigSite.PossibleTreasures.Length; i++)
            totalChance += _myDigSite.PossibleTreasures[i].Chance;

        //Randomizo hasta el techo
        var actualChance = Random.Range(0, totalChance);

        //Me fijo cual toco
        for (int i = 0; i < _myDigSite.PossibleTreasures.Length; i++)
        {
            if (actualChance < _myDigSite.PossibleTreasures[i].Chance)
            {
                DigSceneManager.Instance.ObtainNewTreasure(_myDigSite.PossibleTreasures[i].Treasure);
                break;
            }
        }
    }
}