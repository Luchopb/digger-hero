﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapDigSite : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _timeText;
    
    private ZoneController _myZone;
    private CharacterBehaviour _myActiveCharacter;
    private SpriteRenderer _mySprite;
    private DigSiteType _myDigSiteType;

    private bool _discovered;
    private bool _reached;
    private bool _occupied;

    public bool Discovered => _discovered;
    public bool Reached => _reached;
    public bool Occupied => _occupied;
    public DigSiteType MyDigSiteType => _myDigSiteType;
    public SpriteRenderer MySprite => _mySprite;

    private void Awake()
    {
        _mySprite = GetComponent<SpriteRenderer>();
        _timeText.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    private void Start()
    {
        _timeText.text = "";
    }

    private void Update()
    {
        if (_occupied)
            _timeText.text = _myActiveCharacter.TimeText.text;
    }

    public void SetMapDigSite(DigSiteType dType, ZoneController zone, bool active)
    {
        _myZone = zone;
        if (dType != null)
        {
            _myDigSiteType = dType;
            _discovered = active;
            gameObject.SetActive(_discovered);
        }
    }

    public void Unlock()
    {
        _discovered = true;
        gameObject.SetActive(true);
        _mySprite.sprite = DataBaseManager.Instance.CrossSprite;
    }
    
    private void OnMouseDown()
    {
        if(EventSystem.current.IsPointerOverGameObject() || ZoneManager.Instance.ChangingZone) return;
        
        if(!_occupied)
            ZoneManager.Instance.ShowDigSiteInfo(this, transform.localPosition.y);
        else
            DigSceneManager.Instance.ChangeDigScene(_myDigSiteType, _myActiveCharacter);
    }

    public void OccupyDigSite(CharacterBehaviour character)
    {
        _occupied = true;
        _mySprite.sprite = DataBaseManager.Instance.PickAxeSprite;
        _myActiveCharacter = character;
    }

    public void ClearDigSite()
    {
        _occupied = false;
        _mySprite.sprite = DataBaseManager.Instance.CrossSprite;
        _timeText.text = "";
        _myActiveCharacter = null;
    }
}
