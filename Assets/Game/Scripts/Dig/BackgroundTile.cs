﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BackgroundTile : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _frontSprite;
    [SerializeField] private SpriteRenderer _backSprite;
    [SerializeField] private GameObject _backgroundObject;

    private int _actualId;
    private int _actualMaxId;
    private float _spriteHeight;

    //Fondos posibles
    private Sprite _notDugSprite;
    private Sprite _dugSprite;
    private Sprite _baseSprite;


    public int ActualId
    {
        get => _actualId;
        set
        {
            _actualId = value;
            UpdateSprites();
        }
    }

    public float SpriteHeight => _spriteHeight;

    private void Awake()
    {
        _spriteHeight = _frontSprite.bounds.size.y;
    }

    //Seteo inicial del fondo
    public void SetBackground(Sprite front, Sprite back, Sprite baseSprite, int id, int maxId, float yPosition)
    {
        _notDugSprite = front;
        _dugSprite = back;
        _baseSprite = baseSprite;
        _actualMaxId = maxId;
        _actualId = id;
        
        transform.localPosition = new Vector3(0, yPosition, 0);
        
        UpdateSprites();

        _backSprite.sprite = _dugSprite;
    }

    //Lo reposiciona y actualiza sus ids si sube o baja
    public void Reposition(bool goingDown, Vector3 newPosition)
    {
        if (goingDown)
            _actualId += 3;
        else
            _actualId -= 3;

        UpdateSprites();
        transform.localPosition = newPosition;
    }
    
    //Updatea los sprites en base a la id
    public void UpdateSprites()
    {
        if (_actualId < 0)
        {
            gameObject.SetActive(false);
        }
        else if(_actualId > 0)
        {
            gameObject.SetActive(true);
            _backgroundObject.SetActive(false);
            _backSprite.gameObject.SetActive(true);
            _frontSprite.sprite = _notDugSprite; 
        }
        else
        {
            gameObject.SetActive(true);
            _backgroundObject.SetActive(true);
            _backSprite.gameObject.SetActive(false);
            _frontSprite.sprite = _baseSprite;
        }
    }
}
