﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DigSceneManager : MenuScreen
{
    public static DigSceneManager Instance { get; private set; }
    
    [Header("Visual Settings")] 
    [SerializeField] private SpriteRenderer _skySprite;
    [SerializeField] private SpriteRenderer _backgroundSprite;
    [SerializeField] private TreasureBehaviour _treasureBehaviour;
    [SerializeField] private Animator _activeCharacterAnimator;
    [Header("Parallax Settings")] 
    [SerializeField] private List<BackgroundTile> _backgrounds;
    [SerializeField] private Sprite _frontSprite;
    [SerializeField] private Sprite _backSprite;
    [SerializeField] private Sprite _baseSprite;
    [SerializeField] private float _minDragDistance;
    [SerializeField] private float _snapSpeed;
    [Header("Canvas Settings")] 
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _playerButton;
    
    private DigSiteType _actualDigType;
    private CharacterBehaviour _actualCharacter;
    private Camera _mainCamera;
    private float _swingsPerSprite;
    private float _bottomDepth;
    private float _topDepth;
    private float _centerScreenDepth;

    private int _maxIndex;
    
    private Vector3 _initialDragPosition;
    private float _initialTime;
    private float _touchDistance;
    private float _actualSpeed;
    private int _snapDirection;
    
    private bool _moving;
    private bool _dragging;
    private bool _snapping;
    private bool _followingPlayer;
    
    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        
        _backButton.onClick.AddListener(GoBackToMenu);
        _playerButton.onClick.AddListener(FollowPlayer);
    }

    protected override void Start()
    {
        base.Start();
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        if (!Active || _actualCharacter == null) return;
        
        if(Input.GetMouseButtonDown(0))
            _actualCharacter.Dig();
        
        if (_snapping)
        {
            for (int i = 0; i < _backgrounds.Count; i++)
                _backgrounds[i].transform.localPosition += Time.deltaTime * _snapSpeed * _snapDirection * Vector3.up;

            _snapping = CameraOutOfBounds();
            
            CheckForReposition(_snapDirection);
        }
        else
        {
            //Mobile input
#if !UNITY_EDITOR && UNITY_ANDROID
        if (Input.touches.Length > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                _initialDragPosition = Input.GetTouch(0).position;
                _moving = false;
                _actualSpeed = 0;
            }
            if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (_dragging)
                    DragBackgrounds(Input.GetTouch(0).position);
                else
                    _dragging = Mathf.Abs(_initialDragPosition.y - Input.GetTouch(0).position.y) > _minDragDistance;
            }

            if(Input.GetTouch(0).phase == TouchPhase.Ended && _dragging)
                ReleaseDrag(Input.GetTouch(0).position);
        }
#elif UNITY_EDITOR
            //Editor Input
            if (Input.GetMouseButtonDown(0))
            {
                _initialDragPosition = Input.mousePosition;
                _moving = false;
                _actualSpeed = 0;
            }

            if (Input.GetMouseButton(0))
            {
                if (_dragging)
                    DragBackgrounds(Input.mousePosition);
                else
                    _dragging = Mathf.Abs(_initialDragPosition.y - Input.mousePosition.y) > _minDragDistance;
            }

            if (Input.GetMouseButtonUp(0) && _dragging)
                ReleaseDrag(Input.mousePosition);
#endif
        }

        //Si se mueve por arrastre, avanzan los fondos en esa direccion
        if (_moving)
        {
            //Si me paso por abajo o por arriba, me freno
            if ((_backgrounds[1].ActualId >= _maxIndex && _actualSpeed > 0) || 
                (_backgrounds[1].ActualId <= 0 && _actualSpeed < 0))
            {
                _actualSpeed = 0;
                _moving = false;
                _snapping = CameraOutOfBounds();
            }
            else
            {
                for (int i = 0; i < _backgrounds.Count; i++)
                    _backgrounds[i].transform.localPosition += Time.deltaTime * _actualSpeed * Vector3.up;

                //Voy reduciendo la velocidad, en base a si sube o baja
                if (_actualSpeed > 0)
                    _actualSpeed -= 0.5f;
                else
                    _actualSpeed += 0.5f;

                //me freno si la velocidad es muy lenta
                if (Mathf.Abs(_actualSpeed) < 0.5f)
                    _moving = false;

                //Chequeo si tengo que reacomodar fondos
                CheckForReposition(_actualSpeed);
            }
        }

        //forma temporal de ir al personaje
        if (_followingPlayer && _actualCharacter.Depth != _centerScreenDepth)
            GoToDepth(_actualCharacter.Depth);

        if (Input.GetKeyDown(KeyCode.Space))
            FollowPlayer();

        //Si el player esta en rango de vision, lo acomoda donde va, si no lo mueve a un costado
        if (SpriteInRange(_actualCharacter.Depth))
        {
            _activeCharacterAnimator.transform.position = 
                new Vector3(0, CalculateSpritePosition(CalculateBackgroundForSprite(_actualCharacter.Depth), _actualCharacter.Depth));
            
            if(Mathf.Abs(_activeCharacterAnimator.transform.position.y - _mainCamera.transform.position.y) < 3f)
                FollowPlayer();
        }
        else
            _activeCharacterAnimator.transform.position =
                _actualCharacter.Depth > _bottomDepth ? new Vector3(0, -20f) : new Vector3(0, 100f);

        //Chequeo si el tesoro esta en rango
        if (SpriteInRange(_actualDigType.TimeToReach))
        {
            _treasureBehaviour.transform.position =
                new Vector3(0, CalculateSpritePosition(CalculateBackgroundForSprite(_actualDigType.TimeToReach), _actualDigType.TimeToReach));
        }
        else
            _treasureBehaviour.transform.position = new Vector3(0,  100f);
    }

    private void FollowPlayer()
    {
        _followingPlayer = true;
        _moving = false;
        _playerButton.gameObject.SetActive(false);
    }
    
    private bool CameraOutOfBounds()
    {
        if (SpriteInRange(0) && 
            CalculateSpritePosition(_backgrounds[1], 0) < 0)
        {
            _snapDirection = 1;
            return true;
        }

        if (SpriteInRange(_actualDigType.TimeToReach) &&
            CalculateSpritePosition(_backgrounds[1], _actualDigType.TimeToReach) > 0)
        {
            _snapDirection = -1;
            return true;
        }

        _snapDirection = 0;
        return false;
    }
    
    private BackgroundTile CalculateBackgroundForSprite(float depth)
    {
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            var auxDepth = (_backgrounds[i].ActualId + 1) * _swingsPerSprite;
            if (depth <= auxDepth && depth >= auxDepth - _swingsPerSprite)
            {
                return _backgrounds[i];
            }
        }
        return null;
    }

    private bool SpriteInRange(float depth)
    {
        //Si la profundidad del player esta entre el minimo y el maximo, retorna true
        _bottomDepth = (_backgrounds[0].ActualId + 1) * _swingsPerSprite - _swingsPerSprite;
        _topDepth = (_backgrounds[_backgrounds.Count - 1].ActualId + 1) * _swingsPerSprite;
        
        return depth <= _topDepth && depth >= _bottomDepth;
    }

    //Chequeo si tengo que reacomodar fondos, en base a la dirección que se mueve
    private void CheckForReposition(float direction)
    {
        //Si el fondo del centro se aleja más de su ancho del centro de la pantalla, reacomoda
        if (Vector2.Distance(_backgrounds[1].transform.position,
                _mainCamera.transform.position) > _backgrounds[0].SpriteHeight - 2.5f)
        {
            if (direction > 0)
            {
                var auxGround = _backgrounds[0];
                auxGround.Reposition(true, _backgrounds[_backgrounds.Count - 1].transform.localPosition - new Vector3(0, _backgrounds[0].SpriteHeight));
                _backgrounds.RemoveAt(0);
                _backgrounds.Add(auxGround);
            }
            else
            {
                var auxGround = _backgrounds[_backgrounds.Count - 1];
                auxGround.Reposition(false, _backgrounds[0].transform.localPosition + new Vector3(0, _backgrounds[0].SpriteHeight));
                _backgrounds.RemoveAt(_backgrounds.Count - 1);
                _backgrounds.Insert(0, auxGround);
            }
        }
    }

    //Va a una profundidad especifica
    public void GoToDepth(float depth)
    {
        var groundIndex = (int)(depth / _swingsPerSprite);

        var floor = (groundIndex + 1) * _swingsPerSprite - _swingsPerSprite;
        var top = (groundIndex + 1) * _swingsPerSprite;
        var percentageDepth = (depth - floor) / (top - floor);

        //Offset según la posición porcentual del player en el fondo central
        var yPosition = _backgrounds[0].SpriteHeight * percentageDepth - _backgrounds[0].SpriteHeight / 2;
        
        //print(depth + "m out of " + top + "m, on index " + groundIndex + ". That means " + percentageDepth + "%");
        //print("Player position is " + yPosition);
        groundIndex--;
        //Acomodo las ids y las posiciones de los fondos
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].ActualId = groundIndex + i;
            _backgrounds[i].transform.localPosition = new Vector3(0,_backgrounds[i].SpriteHeight  * -(i-1) + yPosition, 0);
        }

        _centerScreenDepth = depth;
        _actualSpeed = 0;
    }
    
    //Calcula la posicion del player en base al index en que se encuentra y su profundidad
    private float CalculateSpritePosition(BackgroundTile actualGround, float depth)
    {
        var floor = (actualGround.ActualId + 1) * _swingsPerSprite - _swingsPerSprite;
        var top = (actualGround.ActualId + 1) * _swingsPerSprite;
        //Es el porcentaje posicional del fondo en que se encuentra
        var percentageDepth = (depth - floor) / (top - floor);
        
        var groundBottom = actualGround.transform.position.y + actualGround.SpriteHeight / 2;
        
        //Calcula su posicion en y en base al porcentaje
        var yPosition = groundBottom - actualGround.SpriteHeight * percentageDepth;
        
        return yPosition;
    }
    
    //Arrastra los fondos a la posicion del mouse y reacomoda
    private void DragBackgrounds(Vector3 mousePosition)
    {
        //La distancia en mundo del mouse inicial al objeto a mover
        var initialWorldPosition = _mainCamera.ScreenToWorldPoint(_initialDragPosition);
        _followingPlayer = false;
        _playerButton.gameObject.SetActive(true);
        
        _touchDistance = _mainCamera.ScreenToWorldPoint(mousePosition).y - initialWorldPosition.y;

        for (int i = 0; i < _backgrounds.Count; i++)
            _backgrounds[i].transform.localPosition = new Vector3(0, _backgrounds[i].transform.localPosition.y + _touchDistance,0);
        
        CheckForReposition(_touchDistance);

        _initialTime = Time.time;
        _initialDragPosition = mousePosition;
    }

    //Al soltar el arrastre, mantiene la velocidad
    public void ReleaseDrag(Vector3 mousePosition)
    {
        //La distancia en mundo del mouse inicial al objeto a mover
        var initialWorldPosition = _mainCamera.ScreenToWorldPoint(_initialDragPosition);
        _touchDistance = _mainCamera.ScreenToWorldPoint(mousePosition).y - initialWorldPosition.y;

        var elapsedTime = Time.time - _initialTime;
        var speed = _touchDistance / elapsedTime;

        //TODO: ver si queda
        var borderDistance = 0f;

        if (speed < 0)
        {
            borderDistance = (_backgrounds[1].ActualId + 1) * _swingsPerSprite;
        }
        else
        {
            borderDistance = (_maxIndex + 1 - _backgrounds[1].ActualId) * _swingsPerSprite;
        }

        _moving = true;
        _actualSpeed = Mathf.Min(speed, borderDistance);
        _dragging = false;

        _snapping = CameraOutOfBounds();
    }
/*
    //Seteo inicial de la escena de excavacion
    public void StartDigScene(DigSiteType dType, CharacterBehaviour character)
    {
        _actualCharacter = character;
        _activeCharacterAnimator.runtimeAnimatorController = _actualCharacter.MyCharacter.CharacterAnimator;
        //Setea el digsite, los swings que cuesta pasar un tile, y el index maximo de fondos
        _actualDigType = dType;
        _swingsPerSprite = _backgrounds[0].SpriteHeight / _actualCharacter.MyCharacter.DigLength;
        
        _maxIndex = Mathf.RoundToInt(_actualDigType.TimeToReach / _swingsPerSprite);

        //Setea los fondos en base al digsite
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].SetBackground(_frontSprite, _backSprite, _baseSprite,
                i - 1, _maxIndex, -_backgrounds[0].SpriteHeight * (i - 1));
        }
        //Le avisa al manager de menus que debe salir del menu principal
        MainMenuManager.Instance.LeaveMenuScreen(this);
    }*/

    public void ReachTreasure()
    {
        _treasureBehaviour.Reach(_actualDigType);
    }
    
    public void ChangeDigScene(DigSiteType dType, CharacterBehaviour character)
    {
        if (_actualCharacter == character) return;
        
        if(_actualCharacter !=null) 
            _actualCharacter.HideCharacter();
        
        //Cambio los datos de la escena y chequeo si ya fue alcanzado el tesoro
        _actualDigType = dType;
        _actualCharacter = character;
        _treasureBehaviour.ChangeDigSite(_actualCharacter.Depth >= _actualDigType.TimeToReach, _actualDigType);
        _activeCharacterAnimator.runtimeAnimatorController = character.MyCharacter.CharacterAnimator;
        
        _swingsPerSprite = _backgrounds[0].SpriteHeight / _actualCharacter.MyCharacter.DigLength;
        _maxIndex = Mathf.RoundToInt(_actualDigType.TimeToReach / _swingsPerSprite);
        
        //Setea los fondos en base al digsite
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].SetBackground(_frontSprite, _backSprite, _baseSprite,
                i - 1, _maxIndex, -_backgrounds[0].SpriteHeight * (i - 1));
        }
        //Llevo la camara al personaje
        GoToDepth(_actualCharacter.Depth);
        FollowPlayer();
        
        if(!Active)
            MainMenuManager.Instance.LeaveMenuScreen(this);
    }

    public void ObtainNewTreasure(TreasureType treasure)
    {
        ZoneManager.Instance.FinishDigSite(_actualDigType.ZoneIndex, _actualDigType.CaveIndex + 1);
        _actualCharacter.CleanCharacter();
        GameManager.Instance.ObtainTreasure(treasure);
        RewardScreenManager.Instance.ShowTreasureReward(treasure);
    }
    
    //Vuelve al menu principal
    private void GoBackToMenu()
    {
        MainMenuManager.Instance.ReturnToMenuScreen();
    }
    
    public override void TurnOff()
    {
        _actualCharacter = null;
        _actualSpeed = 0;
        base.TurnOff();
    }
}
