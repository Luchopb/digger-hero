﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneController : MonoBehaviour
{
    [SerializeField] private ZoneType _zoneType;
    [SerializeField] private MapDigSite[] _mapDigSites;
    [SerializeField] private DigSiteType[] _digSites;
    [SerializeField] private LineRenderer _digConnector;

    private bool _unlocked;
    private bool _moving;
    
    public bool Unlocked => _unlocked;
    public ZoneType ZoneType => _zoneType;
    public DigSiteType[] DigSites => _digSites;
    public MapDigSite[] MapMapDigSites => _mapDigSites;

    public bool Moving
    {
        get => _moving;
        set => _moving = value;
    }

    private void Start()
    {
        var digSitesData = SaveDataManager.Instance.GetZoneDigSites(_zoneType.ZoneName);
        //TODO: que solo 
        if (digSitesData == null) return;
        
        var positions = new List<Vector3>();
        for (var i = 0; i < _digSites.Length; i++)
        {
            _mapDigSites[i].SetMapDigSite(_digSites[i], this,digSitesData[i].Unlocked);
            if(digSitesData[i].Unlocked)
                positions.Add(_mapDigSites[i].transform.localPosition);
        }
        
        _digConnector.positionCount = positions.Count;
        _digConnector.SetPositions(positions.ToArray());
    }

    public void FinishDigSite(int caveIndex)
    {
        _mapDigSites[caveIndex - 1].ClearDigSite();

        if (_mapDigSites[caveIndex].Discovered) return;
        
        _mapDigSites[caveIndex].Unlock();
        
        UpdateTreasureLine();
    }

    private void Update()
    {
        if (_moving)
        {
            for (int i = 0; i < _mapDigSites.Length; i++)
            {
                if (!_mapDigSites[i].Discovered) continue;

                _digConnector.SetPosition(i, _mapDigSites[i].transform.localPosition);
            }
        }
    }

    private void UpdateTreasureLine()
    {
        var positions = new List<Vector3>();
        //TODO: que se agregue la linea al array del linerenderer
        for (int i = 0; i < _mapDigSites.Length; i++)
        {
            if (!_mapDigSites[i].Discovered) continue;

            positions.Add(_mapDigSites[i].transform.localPosition);
        }

        _digConnector.positionCount = positions.Count;
        _digConnector.SetPositions(positions.ToArray());
    }
    
    public void OccupyDigSite(int caveIndex, CharacterBehaviour character)
    {
        _mapDigSites[caveIndex].OccupyDigSite(character);
    }
    
    public DigSiteSaveData[] GenerateDigSitesData()
    {
        var digSitesData = new DigSiteSaveData[_mapDigSites.Length];
        
        for (int i = 0; i < _mapDigSites.Length; i++)
            digSitesData[i] = new DigSiteSaveData(_zoneType.ZoneName, i,  
                i == 0 || _mapDigSites[i].Discovered, _mapDigSites[i].Reached);

        return digSitesData;
    }
}
