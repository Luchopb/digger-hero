﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSaveData
{
    private int _obtainedGold;
    private int _maxGold;
    private int _prestigeLevel;
    private string _dateTime;
    private CharacterSaveData[] _charactersData;
    private PickAxeSaveData[] _obtainedPickAxes;
    private MaterialSaveData[] _obtainedMaterials;
    private MuseumSaveData _museumData;
    private TreasureSaveData[] _obtainedTreasures;
    private DigZoneSaveData[] _digZonesData;
    
    public int ObtainedGold
    {
        get => _obtainedGold;
        set => _obtainedGold = value;
    }
    public int MaxGold
    {
        get => _maxGold;
        set => _maxGold = value;
    }

    public int PrestigeLevel
    {
        get => _prestigeLevel;
        set => _prestigeLevel = value;
    }

    public string DateTime => _dateTime;
    public CharacterSaveData[] CharacterData => _charactersData;
    public PickAxeSaveData[] ObtainedPickAxes => _obtainedPickAxes;
    public MaterialSaveData[] ObtainedMaterials => _obtainedMaterials;
    public MuseumSaveData MuseumData => _museumData;
    public TreasureSaveData[] ObtainedTreasures => _obtainedTreasures;
    public DigZoneSaveData[] DigZonesData => _digZonesData;

    public GameSaveData(CharacterSaveData[] cData, PickAxeSaveData[] pData, MaterialSaveData[] matData,
        MuseumSaveData mData, TreasureSaveData[] tData, DigZoneSaveData[] digData, 
        int gold, int maxGold, int prestige)
    {
        _dateTime = System.DateTime.Now.ToBinary().ToString();
        
        _obtainedGold = gold;
        _maxGold = maxGold;
        _prestigeLevel = prestige;
        _charactersData = cData;
        _obtainedPickAxes = pData;
        _obtainedMaterials = matData;
        _museumData = mData;
        _obtainedTreasures = tData;
        _digZonesData = digData;
    }
}
