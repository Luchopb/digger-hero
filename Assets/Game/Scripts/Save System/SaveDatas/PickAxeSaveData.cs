﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PickAxeSaveData
{
    private string _materialName;
    private int _pickAxeLevel;
    private int _actualUpgradeCost;
    private int _actualMaterialCost;

    public string MaterialName => _materialName;
    public int ActualUpgradeCost => _actualUpgradeCost;
    public int ActualMaterialCost => _actualMaterialCost;
    public int PickAxeLevel
    {
        get => _pickAxeLevel;
        set => _pickAxeLevel = value;
    }

    public PickAxeSaveData(string mat, int level, int cost, int mCost)
    {
        _materialName = mat;
        _pickAxeLevel = level;
        _actualUpgradeCost = cost;
        _actualMaterialCost = mCost;
    }
}
