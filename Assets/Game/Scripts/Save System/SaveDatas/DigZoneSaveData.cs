﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DigZoneSaveData
{
    private string _zoneName;
    private DigSiteSaveData[] _digSites;
    private bool _unlocked;

    public string ZoneName => _zoneName;
    public DigSiteSaveData[] DigSites => _digSites;
    public bool Unlocked
    {
        get => _unlocked;
        set => _unlocked = value;
    }

    public DigZoneSaveData(string zoneName, DigSiteSaveData[] digSites, bool unlocked)
    {
        _zoneName = zoneName;
        _digSites = digSites;
        _unlocked = unlocked;
    }
}
