﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DigSiteSaveData
{
    private string _zoneName;
    private int _siteIndex;
    private bool _unlocked;
    private bool _reached;

    public string ZoneName => _zoneName;
    public int SiteIndex => _siteIndex;
    public bool Unlocked
    {
        get => _unlocked;
        set => _unlocked = value;
    }
    public bool Reached
    {
        get => _reached;
        set => _reached = value;
    }

    public DigSiteSaveData(string name, int index,
        bool unlocked, bool reached)
    {
        _zoneName = name;
        _siteIndex = index;
        _unlocked = unlocked;
        _reached = reached;
    }
}
