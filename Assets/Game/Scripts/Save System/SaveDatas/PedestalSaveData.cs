﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PedestalSaveData
{
    private int _level;
    private int _index;
    private int _actualUpgradeCost;
    private string _treasureName;
    private bool _treasureObtained;

    public int Index => _index;
    public int ActualUpgradeCost => _actualUpgradeCost;
    public string TreasureName => _treasureName;
    public int Level
    {
        get => _level;
        set => _level = value;
    }
    public bool TreasureObtained
    {
        get => _treasureObtained;
        set => _treasureObtained = value;
    }

    public PedestalSaveData(string tName, int level, int index,
    int cost, bool tObtained)
    {
        _treasureName = tName;
        _level = level;
        _index = index;
        _actualUpgradeCost = cost;
        _treasureObtained = tObtained;
    }
}
