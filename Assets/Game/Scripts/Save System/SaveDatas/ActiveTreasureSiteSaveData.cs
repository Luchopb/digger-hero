﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ActiveTreasureSiteSaveData
{
    private int _zoneIndex;
    private int _caveIndex;
    private float _playerDepth;
    private float _maxDepth;

    public int ZoneIndex
    {
        get => _zoneIndex;
        set => _zoneIndex = value;
    }
    public int CaveIndex
    {
        get => _caveIndex;
        set => _caveIndex = value;
    }
    public float PlayerDepth
    {
        get => _playerDepth;
        set => _playerDepth = value;
    }
    public float MaxDepth
    {
        get => _maxDepth;
        set => _maxDepth = value;
    }

    public ActiveTreasureSiteSaveData(int sIndex, int cIndex, float pDepth, float mDepth)
    {
        _zoneIndex = sIndex;
        _caveIndex = cIndex;
        _playerDepth = pDepth;
        _maxDepth = mDepth;
    }
}
