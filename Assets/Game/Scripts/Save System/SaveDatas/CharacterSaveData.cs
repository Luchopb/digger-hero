﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterSaveData
{
    private string _characterName;
    private bool _digging;
    private int _characterIndexMenu;
    private ActiveTreasureSiteSaveData _activeSite;
    
    public bool Digging
    {
        get => _digging;
        set => _digging = value;
    }
    public ActiveTreasureSiteSaveData ActiveSite
    {
        get => _activeSite;
        set => _activeSite = value;
    }
    public int CharacterIndexMenu
    {
        get => _characterIndexMenu;
        set => _characterIndexMenu = value;
    }

    public string CharacterName => _characterName;

    public CharacterSaveData(string name, bool digging, int index, ActiveTreasureSiteSaveData site)
    {
        _characterName = name;
        _digging = digging;
        _characterIndexMenu = index;
        _activeSite = site;
    }
}
