﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TreasureSiteSaveData
{
    public string _zone;
    private float _depth;
    private bool _reached;

    public string Zone => _zone;
    public float Depth => _depth;
    public bool Reached => _reached;

    public TreasureSiteSaveData(string zone, float depth, bool reached)
    {
        _zone = zone;
        _depth = depth;
        _reached = reached;
    }
}
