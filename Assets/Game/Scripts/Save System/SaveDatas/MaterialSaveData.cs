﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialSaveData
{
    private int _materialIndex;
    private int _amount;

    public int MaterialIndex => _materialIndex;
    public int Amount => _amount;

    public MaterialSaveData(int mat, int amount)
    {
        _materialIndex = mat;
        _amount = amount;
    }
}
