﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TreasureSaveData
{
    private string _treasureName;
    private int _level;
    private int _amount;

    public string TreasureName => _treasureName;
    public int Level => _level;
    public int Amount => _amount;

    public TreasureSaveData(string name, int level, int amount)
    {
        _treasureName = name;
        _level = level;
        _amount = amount;
    }
}
