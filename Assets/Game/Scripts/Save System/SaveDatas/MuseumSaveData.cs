﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MuseumSaveData
{
    private int _museumLevel;
    private int _receptionLevel;
    private int _safeLevel;
    private int _goldPerTicket;
    private List<PedestalSaveData> _pedestals;

    public int GoldPerTicket => _goldPerTicket;
    public List<PedestalSaveData> Pedestals => _pedestals;
    public int MuseumLevel
    {
        get => _museumLevel;
        set => _museumLevel = value;
    }
    public int SafeLevel
    {
        get => _safeLevel;
        set => _safeLevel = value;
    }
    public int ReceptionLevel
    {
        get => _receptionLevel;
        set => _receptionLevel = value;
    }

    public MuseumSaveData(int level, int rLevel, int sLevel,
        int goldTicket, List<PedestalSaveData> pedestals)
    {
        _museumLevel = level;
        _receptionLevel = rLevel;
        _safeLevel = sLevel;
        _goldPerTicket = goldTicket;
        _pedestals = pedestals;
    }
}
