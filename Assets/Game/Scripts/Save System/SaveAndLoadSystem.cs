﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class SaveAndLoadSystem : MonoBehaviour
{
    public static SaveAndLoadSystem Instance { get; private set; }

    private string _path;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        _path = Path.Combine(Application.persistentDataPath, "save.dat");
    }

    public void SaveGameData(GameSaveData gData)
    {
        if (gData == null) return;
        
        BinaryFormatter formatter = new BinaryFormatter();
        
        FileStream stream = new FileStream(_path, FileMode.Create);
        formatter.Serialize(stream, gData);
        stream.Close();
    }

    public GameSaveData LoadGameData()
    {
        if (!File.Exists(_path)) return null;
        
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(_path, FileMode.Open);
        
        var gData = formatter.Deserialize(stream) as GameSaveData;
        stream.Close();

        return gData;
    }
}
