﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MuseumManager : MenuScreen
{
    public static MuseumManager Instance { get; private set; }
    
    [SerializeField] private List<PedestalBehaviour> _pedestals;

    [Header("NAVIGATION")] 
    [SerializeField] private Transform _roomsContainer;
    [SerializeField] private SpriteRenderer _sizeReferenceSprite;
    [SerializeField] private RoomData[] _rooms;
    [Space(10)]
    [SerializeField] private Material _outlineMaterial;
    [SerializeField] private Material _normalMaterial;

    [Header("CANVAS SETTINGS")] 
    [SerializeField] private Button _leftButton;
    [SerializeField] private Button _rightButton;
    [SerializeField] private Button _upButton;
    [SerializeField] private Button _downButton;
    [Space(5)] 
    [SerializeField] private Transform _selectedTreasurePositioner;
    [SerializeField] private Transform _selectedTreasureContainer;
    [SerializeField] private Image _selectedTreasureImage;
    [SerializeField] private TextMeshProUGUI _treasureName;
    [SerializeField] private Text _treasureDescription;
    [SerializeField] private Text _treasureRarity;
    [SerializeField] private Button _closeInfoButton;

    private Transform[,] _roomPositions;
    
    private int _prestigeLevel;
    private int _receptionLevel;
    private int _vaultLevel;
    private int _goldPerTicket;

    private float _spriteWidth;
    private float _spriteHeight;
    
    private int _minXIndex = 0;
    private int _maxXIndex;
    private int _actualXIndex;
    private int _minYIndex = 0;
    private int _maxYIndex;
    private int _actualYIndex;
    private Transform _actualRoom;
    
    private SpriteRenderer _actualSelectedTreasure;
    private Vector3 _selectedTreasureContainerInitialPosition;
    
    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        _closeInfoButton.onClick.AddListener(HideTreasureInfo);
        _leftButton.onClick.AddListener(() => NavigateMuseum(SwipeDirection.Left));
        _rightButton.onClick.AddListener(() => NavigateMuseum(SwipeDirection.Right));
        _downButton.onClick.AddListener(() => NavigateMuseum(SwipeDirection.Down));
        _upButton.onClick.AddListener(() => NavigateMuseum(SwipeDirection.Up));

        _spriteWidth = _sizeReferenceSprite.bounds.size.x;
        _spriteHeight = _sizeReferenceSprite.bounds.size.y;
        _actualXIndex = 1;
        _actualYIndex = 0;
        
        //Seteo el maximo de x e y
        for (int i = 0; i < _rooms.Length; i++)
        {
            if (_rooms[i].XIndex > _maxXIndex) 
                _maxXIndex = _rooms[i].XIndex;
            
            if (_rooms[i].YIndex > _maxYIndex) 
                _maxYIndex = _rooms[i].YIndex;
        }
        
        //Creo el array bidimensional
        _roomPositions = new Transform[_maxXIndex + 1,_maxYIndex + 1];
        
        //Lo relleno con la información
        for (int i = 0; i < _rooms.Length; i++)
        {
            _roomPositions[_rooms[i].XIndex, _rooms[i].YIndex] = _rooms[i].Room;
            //Si no es el inicial, los apago
            if(_rooms[i].XIndex != _actualXIndex || _rooms[i].YIndex != _actualYIndex)
                _roomPositions[_rooms[i].XIndex, _rooms[i].YIndex].gameObject.SetActive(false);
            
            _roomPositions[_rooms[i].XIndex, _rooms[i].YIndex].localPosition = new Vector2(_spriteWidth * _rooms[i].XIndex, -_spriteHeight * _rooms[i].YIndex);
        }
        _roomsContainer.localPosition = new Vector2(-_spriteWidth * _actualXIndex, -_spriteHeight * _actualYIndex);
        _actualRoom = _roomPositions[_actualXIndex, _actualYIndex];
        
        _leftButton.gameObject.SetActive(_actualXIndex > _minXIndex);
        _rightButton.gameObject.SetActive(_actualXIndex < _maxXIndex);
        _downButton.gameObject.SetActive(_actualYIndex < _maxYIndex);
        _upButton.gameObject.SetActive(_actualYIndex > _minYIndex);
    }

    protected override void Start()
    {
        _selectedTreasureContainerInitialPosition = _selectedTreasureContainer.localPosition;
    }

    public MuseumSaveData GenerateMuseumSaveData()
    {
        var pData = new List<PedestalSaveData>();
        
        for (int i = 0; i < _pedestals.Count; i++)
            pData.Add(_pedestals[i].GeneratePedestalSaveData());
        
        var mData = new MuseumSaveData(_prestigeLevel, _receptionLevel, _vaultLevel,
            _goldPerTicket, pData);
        
        return mData;
    }

    public void ObtainTreasure(string treasure, bool inGame = true)
    {
        //TODO: con el treasuretype genero el pedestal nuevo?
        for (int i = 0; i < _pedestals.Count; i++)
        {
            if (_pedestals[i].MyTreasure.TreasureName != treasure) continue;
            
            _pedestals[i].ShowTreasure();
            break;
        }
        if(inGame)
            SaveDataManager.Instance.ObtainNewTreasure(treasure);
    }

    private void NavigateMuseum(SwipeDirection direction)
    {
        switch (direction)
        {
            case SwipeDirection.Left:
                if (_actualXIndex > _minXIndex)
                    _actualXIndex--;
                break;
            case SwipeDirection.Right:
                if (_actualXIndex < _maxXIndex)
                    _actualXIndex++;
                break;
            case SwipeDirection.Down:
                if (_actualYIndex < _maxYIndex)
                    _actualYIndex++;
                break;
            case SwipeDirection.Up:
                if (_actualYIndex > _minYIndex)
                    _actualYIndex--;
                break;
            case SwipeDirection.None:
                return;
        }
        
        _leftButton.gameObject.SetActive(_actualXIndex > _minXIndex);
        _rightButton.gameObject.SetActive(_actualXIndex < _maxXIndex);
        _downButton.gameObject.SetActive(_actualYIndex < _maxYIndex);
        _upButton.gameObject.SetActive(_actualYIndex > _minYIndex);

        var auxRoom = _actualRoom;
        
        if(_actualRoom != _roomPositions[_actualXIndex, _actualYIndex])
        {
            _actualRoom = _roomPositions[_actualXIndex, _actualYIndex];
            _actualRoom.gameObject.SetActive(true);
            _roomsContainer.DOLocalMove(new Vector2(-_spriteWidth * _actualXIndex, _spriteHeight * _actualYIndex), .5f
            ).SetEase(Ease.OutQuad).OnComplete(()=> auxRoom.gameObject.SetActive(false));
        }
    }
    
    public void ShowTreasureInfo(TreasureType treasure, SpriteRenderer clickedSprite)
    {
        if(DOTween.IsTweening(_selectedTreasureContainer))
            DOTween.Kill(_selectedTreasureContainer);
        
        _selectedTreasureContainer.DOLocalMoveY(_selectedTreasurePositioner.localPosition.y, 0.5f).SetEase(Ease.OutQuad);
        _selectedTreasureImage.sprite = treasure.TreasureSprite;
        _treasureName.text = treasure.TreasureName;
        _treasureDescription.text = treasure.Description;
        
        //Apago el outline anterior y prendo el nuevo
        if (_actualSelectedTreasure != null)
            _actualSelectedTreasure.material = _normalMaterial;
        
        _actualSelectedTreasure = clickedSprite;
        _actualSelectedTreasure.material = _outlineMaterial;
    }

    public void HideTreasureInfo()
    {
        if(DOTween.IsTweening(_selectedTreasureContainer))
            DOTween.Kill(_selectedTreasureContainer);
        
        if(_actualSelectedTreasure != null)
            _actualSelectedTreasure.material = _normalMaterial;
        _selectedTreasureContainer.DOLocalMoveY(_selectedTreasureContainerInitialPosition.y, 0.5f).SetEase(Ease.OutQuad);
    }

    public void LoadMuseumData(MuseumSaveData mData)
    {
        for (int i = 0; i < mData.Pedestals.Count; i++)
        {
            if (mData.Pedestals[i].TreasureObtained)
                ObtainTreasure(mData.Pedestals[i].TreasureName, false);
        }
    }
}

[System.Serializable]
public class RoomData
{
    [SerializeField] private Transform _room;
    [SerializeField] private int _xIndex;
    [SerializeField] private int _yIndex;

    public Transform Room => _room;
    public int XIndex => _xIndex;
    public int YIndex => _yIndex;
}
