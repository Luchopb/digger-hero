﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PedestalBehaviour : MonoBehaviour
{
    [SerializeField] private TreasureType _myTreasure;
    [SerializeField] private SpriteRenderer _treasureSprite;
    [SerializeField] private SpriteRenderer _glassSprite;

    private SpriteRenderer _pedestalSprite;
    private int _level;
    private int _index;
    private int _actualUpgradeCost;
    private bool _treasureObtained;

    public TreasureType MyTreasure => _myTreasure;

    private void Awake()
    {
        _pedestalSprite = GetComponent<SpriteRenderer>();
    }

    public PedestalSaveData GeneratePedestalSaveData()
    {
        PedestalSaveData dataToReturn = new PedestalSaveData(_myTreasure.TreasureName, _level, _index, _actualUpgradeCost, _treasureObtained);
        return dataToReturn;
    }

    public void ShowTreasure()
    {
        if (!_treasureObtained)
        {
            _treasureSprite.sprite = _myTreasure.TreasureSprite;
            _treasureSprite.gameObject.SetActive(true);
            _treasureObtained = true;
            GameManager.Instance.AddPrestige(1);
        }
        //else
            
    }

    private void OnMouseDown()
    {
        if (!_treasureObtained) return;
        
        MuseumManager.Instance.ShowTreasureInfo(_myTreasure, _treasureSprite);
    }
}
