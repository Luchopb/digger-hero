﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterDisplay : MonoBehaviour
{
    [SerializeField] private CharacterType _characterType;
    [Header("Canvas Settings")]
    [SerializeField] private Image _characterPortrait;
    [SerializeField] private TextMeshProUGUI _characterNameText;
    [SerializeField] private TextMeshProUGUI _characterDescriptionText;
    private bool _digging;
    private CharacterBehaviour _activeCharacter;

    public CharacterType CharacterType => _characterType;
    public bool Digging => _digging;
    public CharacterBehaviour ActiveCharacter => _activeCharacter;

    private void Awake()
    {
        InitialSetup();
    }

    private void InitialSetup()
    {
        _characterPortrait.sprite = _characterType.CharacterUiSprite;
        _characterNameText.text = _characterType.CharacterName;
        _characterDescriptionText.text = _characterType.CharacterDescription;
    }
    
    public void SetDigger(CharacterBehaviour character)
    {
        _digging = true;
        _activeCharacter = character;
    }

    public void ReleaseDigger()
    {
        _digging = false;
        _activeCharacter = null;
    }
}
