﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersManager : MenuScreen
{
    public static CharactersManager Instance { get; private set; }

    [SerializeField] private List<CharacterDisplay> _allCharacters;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
    }

    public CharacterDisplay GetAvailableCharacter()
    {
        for (int i = 0; i < _allCharacters.Count; i++)
        {
            if (!_allCharacters[i].Digging)
                return _allCharacters[i];
        }
        return null;
    }

    public void OccupyCharacter(CharacterBehaviour character, string charName)
    {
        foreach (var characterDisplay in _allCharacters)
        {
            if(characterDisplay.CharacterType.CharacterName == charName)
                characterDisplay.SetDigger(character);
        }
    }

    public void ReleaseCharacter(string charName)
    {
        foreach (var characterDisplay in _allCharacters)
        {
            if(characterDisplay.CharacterType.CharacterName == charName)
                characterDisplay.ReleaseDigger();
        }
    }
    
    public CharacterSaveData[] GenerateCharactersData()
    {
        CharacterSaveData[] cData = new CharacterSaveData[_allCharacters.Count];

        for (int i = 0; i < _allCharacters.Count; i++)
        {
            if (_allCharacters[i].Digging)
            {
                var activeCharacter = _allCharacters[i].ActiveCharacter;
                var digSite = new ActiveTreasureSiteSaveData(activeCharacter.ActiveDigSite.ZoneIndex, activeCharacter.ActiveDigSite.CaveIndex, activeCharacter.Depth,
                    activeCharacter.ActiveDigSite.TimeToReach);
                var character = new CharacterSaveData(_allCharacters[i].CharacterType.CharacterName, 
                    true, activeCharacter.MenuIndex, digSite);
                cData[i] = character;
                print(activeCharacter.MenuIndex);
            }
            else
            {
                var character = new CharacterSaveData(_allCharacters[i].CharacterType.CharacterName,
                    false, -1, null);
                cData[i] = character;
            }
        }
        
        return cData;
    }
}
