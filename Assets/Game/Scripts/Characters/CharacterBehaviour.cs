﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBehaviour : MonoBehaviour
{
    [SerializeField] private Sprite _doneSprite;
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private Image _characterImage;
    [SerializeField] private int _menuIndex;
    
    private Button _characterButton;

    private float _depth;
    private float _timeLeft;
    private bool _digging;
    private bool _visible;
    private CharacterType _myCharacter;
    private DigSiteType _activeDigSite;
    private Animator _animator;

    public int MenuIndex => _menuIndex;
    public float Depth => _depth;
    public bool Digging => _digging;
    public TextMeshProUGUI TimeText => _timeText;
    public CharacterType MyCharacter => _myCharacter;
    public DigSiteType ActiveDigSite => _activeDigSite;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _characterButton = GetComponent<Button>();
    }

    private void Start()
    {
        _characterButton.onClick.AddListener(ToggleCharacterDigSite);
        
        if (_digging)
            UpdateCanvas();
        else
        {
            _characterImage.gameObject.SetActive(false);
            _timeText.text = "-";
        }
    }

    public void ActivateCharacter(DigSiteType digSite, CharacterType character)
    {
        _digging = true;
        _visible = true;
        _characterImage.gameObject.SetActive(true);
        _activeDigSite = digSite;
        _timeLeft = _activeDigSite.TimeToReach / character.DigLength;
        _depth = 0;
        
        //Seteo el personaje y lo pongo a excavar
        _myCharacter = character;

        _characterImage.sprite = character.CharacterUiSprite;
        
        InvokeRepeating(nameof(Dig), 1f, 1f);
        
        UpdateCanvas();
    }

    public void ResumeDigging(DigSiteType digSite, CharacterType character, float depth, float timeDifference)
    {
        _digging = true; 
        _activeDigSite = digSite;
        _depth = depth;
        _timeLeft = (_activeDigSite.TimeToReach - (depth + timeDifference * character.DigLength)) / character.DigLength;

        //Seteo el personaje y lo pongo a excavar
        _myCharacter = character;

        _characterImage.sprite = character.CharacterUiSprite;
        
        if(depth < digSite.TimeToReach)
            InvokeRepeating(nameof(Dig), 1f, 1f);
        else
            ReachTreasure();

        UpdateCanvas();
    }
    
    public void Dig()
    {
        _depth += _myCharacter.DigLength;

        //Calculo los metros (tiempo) que me faltan
        _timeLeft--;
        
        if (_timeLeft <= 0)
            ReachTreasure();
        
        UpdateCanvas();
        SaveDataManager.Instance.UpdateCharacterStatus(_myCharacter.CharacterName, this);
    }

    private void ReachTreasure()
    {
        CancelInvoke(nameof(Dig));
        
        _depth = _activeDigSite.TimeToReach;
        _timeLeft = 0;
        _characterImage.sprite = _doneSprite;
        
        if(_visible)
            DigSceneManager.Instance.ReachTreasure();
    }

    private void UpdateCanvas()
    {
        //Hago el pasaje y lo muestro en pantalla
        if (_timeLeft > 0)
        {
            var minutes = Mathf.FloorToInt(_timeLeft / 60);
            var seconds = Mathf.FloorToInt(_timeLeft - minutes * 60);
            _timeText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
        }
        else
            _timeText.text = "Done!";
    }

    public void CleanCharacter()
    {
        CharactersManager.Instance.ReleaseCharacter(_myCharacter.CharacterName);
        
        _digging = false;
        _visible = false;
        SaveDataManager.Instance.UpdateCharacterStatus(_myCharacter.CharacterName, this);
        
        _myCharacter = null;
        _activeDigSite = null;
        _timeText.text = "-";
        _characterImage.gameObject.SetActive(false);
    }

    public void HideCharacter()
    {
        _visible = false;
    }
    
    private void ToggleCharacterDigSite()
    {
        if (_myCharacter == null) return;
        
        DigSceneManager.Instance.ChangeDigScene(_activeDigSite, this);
        _visible = true;
    }
}
