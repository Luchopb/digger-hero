﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScreen : MonoBehaviour
{
    [SerializeField] private Canvas _myCanvas;

    protected bool Active;
    private bool _set;

    protected virtual void Start()
    {
        if(!_set)
            InitialSetUp();
    }

    public void InitialSetUp()
    {
        if (_myCanvas != null)
        {
            _myCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            Invoke(nameof(ResizeCanvas), 0.01f);
            _set = true;
        }
    }

    private void ResizeCanvas()
    {
        _myCanvas.renderMode = RenderMode.WorldSpace;
        _myCanvas.transform.localPosition = new Vector3(0,0,-2f);
    }
    
    public virtual void TurnOn()
    {
        Active = true;
    }

    public virtual void TurnOff()
    {
        Active = false;
    }
}
