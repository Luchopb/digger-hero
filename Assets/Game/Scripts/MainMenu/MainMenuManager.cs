﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager Instance { get; private set; }

    [SerializeField] private Image[] _screenSelectors;
    [SerializeField] private MenuScreen[] _screens;
    [Space(5)]
    [SerializeField] private float _minDragDistance = 6f;
    [SerializeField] private MenuScreen _initialScreen;
    [SerializeField] private Transform _screensContainer;
    [SerializeField] private Canvas _mainCanvas;
    [Header("Sprites")]
    [SerializeField] private Sprite _selectedSprite;
    [SerializeField] private Sprite _unselectedSprite;

    private Camera _mainCamera;
    private int _actualScreenIndex;
    private float _screenWidth;
    
    private Vector3 _initialDragPosition;
    private float _touchDistance;
    
    private bool _hasToSnap;
    private bool _canSwipe = true;
    private bool _dragging;
    private bool _inMainScreen;
    
    private MenuScreen _activeScreen;
    
    private void Awake()
    {
        if (Instance != null) Destroy(this);

        Instance = this;
    }

    private void Start()
    {
        SwipeDetectionManager.OnSwipe += SetSwipe;
        _mainCamera = Camera.main;
        _inMainScreen = true;

#if !UNITY_EDITOR && UNITY_ANDROID
        _minDragDistance *= 5f;
#endif
    }

    private void Update()
    {
        if (!_canSwipe || !_inMainScreen) return;

#if  !UNITY_EDITOR && UNITY_ANDROID
        if (Input.touches.Length > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                _initialDragPosition = Input.GetTouch(0).position;
            }
            if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (_dragging)
                    DragContainer(Input.GetTouch(0).position);
                else
                    _dragging = Mathf.Abs(_initialDragPosition.x - Input.GetTouch(0).position.x) > _minDragDistance;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                ReleaseContainer();
            }
        }
#elif UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            _initialDragPosition = Input.mousePosition;
        }
        
        if (Input.GetMouseButton(0))
        {
            if(_dragging)
                DragContainer(Input.mousePosition);
            else
                _dragging = Mathf.Abs(_initialDragPosition.x - Input.mousePosition.x) > _minDragDistance;
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            ReleaseContainer();
        }
#endif
    }

    private void DragContainer(Vector3 mousePosition)
    {
        //La distancia en mundo del mouse inicial al objeto a mover
        var initialWorldPosition = _mainCamera.ScreenToWorldPoint(_initialDragPosition);
        _touchDistance = _mainCamera.ScreenToWorldPoint(mousePosition).x - initialWorldPosition.x;
        
        if ((_actualScreenIndex <= 0 && _touchDistance > 0) ||
            (_actualScreenIndex >= _screens.Length - 1 && _touchDistance < 0)) return; 
        
        _screensContainer.transform.position =
            new Vector3(_screensContainer.position.x + _touchDistance, 0,0); //Camera.main.ScreenToWorldPoint(distance).x;

        _initialDragPosition = mousePosition;
    }

    private void ReleaseContainer()
    {
        int newIndex = 0;
        var closestDistance = Vector2.Distance(_mainCamera.transform.position, _screens[0].transform.position);
        for (int i = 1; i < _screens.Length; i++)
        {
            var newDistance = Vector2.Distance(_mainCamera.transform.position, _screens[i].transform.position);
            if (newDistance < closestDistance)
            {
                newIndex = i;
                closestDistance = newDistance;
            }
        }
        
        _dragging = false;
        ChangeActiveScreen(newIndex);
    }
    
    private void SetSwipe(SwipeData swipeData)
    {
        if (!_canSwipe || !_inMainScreen) return;
        
        var tempInt = 0;
        switch (swipeData.Direction)
        {
            case SwipeDirection.Left:
                tempInt = _actualScreenIndex + 1;
                
                if (tempInt >= _screens.Length)
                    tempInt = _screens.Length - 1;
                
                ChangeActiveScreen(tempInt);
                break;
            case SwipeDirection.Right:
                tempInt = _actualScreenIndex - 1;
                
                if (tempInt < 0)
                    tempInt = 0;
                
                ChangeActiveScreen(tempInt);
                break;
        }
    }
    
    public void SetScreens()
    {
        _screenWidth = _screens[0].GetComponent<SpriteRenderer>().bounds.size.x;
        
        for (int i = 0; i < _screens.Length; i++)
        {
            _screens[i].transform.localPosition = new Vector3(_screenWidth * (i - 2), 0, 0);
            
            if (_screens[i] == _initialScreen)
            {
                _activeScreen = _screens[i];
                _actualScreenIndex = i;
            }
            
            _screens[i].InitialSetUp();
        }
        
        _screenSelectors[_actualScreenIndex].sprite = _selectedSprite;
        _screensContainer.position = new Vector2(-_screenWidth * (_actualScreenIndex -2), 0);
    }

    public void ChangeActiveScreen(int newScreenIndex)
    {
        if (newScreenIndex < 0 || newScreenIndex >= _screens.Length) return;

        if(!_inMainScreen)
            ReturnToMenuScreen();
        
        _activeScreen.TurnOff();
        _canSwipe = false;
        _screenSelectors[_actualScreenIndex].sprite = _unselectedSprite;
        _actualScreenIndex = newScreenIndex;
        
        _activeScreen = _screens[_actualScreenIndex];
        
        _screenSelectors[_actualScreenIndex].sprite = _selectedSprite;
        _screensContainer.DOMoveX(-_screenWidth * (_actualScreenIndex -2), 0.2f).OnComplete(() =>
        {
            _canSwipe = true;
            _activeScreen.TurnOn();
        });
    }

    public void LeaveMenuScreen(MenuScreen newScreen)
    {
        _canSwipe = false;
        _inMainScreen = false;
        
        _screensContainer.transform.position = new Vector3(200,200,0);
        _screensContainer.gameObject.SetActive(false);
        newScreen.gameObject.SetActive(true);
        newScreen.transform.position = Vector3.zero;
        
        if(!_screens.Contains(_activeScreen))
            _activeScreen.transform.position = new Vector3(200, 200, 0);
        
        _activeScreen.TurnOff();
        _screenSelectors[_actualScreenIndex].sprite = _unselectedSprite;
        _activeScreen = newScreen;
        _activeScreen.TurnOn();
    }

    public void TurnMainCanvasOff()
    {
        _mainCanvas.gameObject.SetActive(false);
    }
    
    public void ReturnToMenuScreen(int screenIndex = -1)
    {
        _canSwipe = true;
        _inMainScreen = true;
        _activeScreen.TurnOff();

        _mainCanvas.gameObject.SetActive(true);
        _screensContainer.gameObject.SetActive(true);
        _screensContainer.transform.position = Vector3.zero;
        _activeScreen.transform.position = new Vector3(200,200,0);
        _activeScreen.gameObject.SetActive(false);
        
        if (screenIndex >= 0 && screenIndex < _screens.Length)
        {
            _actualScreenIndex = screenIndex;
            _activeScreen = _screens[_actualScreenIndex];
            _activeScreen.TurnOn();
            _screenSelectors[_actualScreenIndex].sprite = _selectedSprite;
            _screensContainer.DOMoveX(-_screenWidth * (_actualScreenIndex - 2), 0f);
        }
    }
}
