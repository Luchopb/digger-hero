﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBaseManager : MonoBehaviour
{
    public static DataBaseManager Instance { get; private set; }

    [Header("CHARACTERS")] 
    [SerializeField] private CharacterType[] _characters;
    [Header("PICKAXES")]
    [SerializeField] private PickAxeType[] _pickAxes;
    [Header("TREASURES")]
    [SerializeField] private TreasureType[] _treasures;
    [Header("Zones")]
    [SerializeField] private ZoneType[] _zones;
    [Header("DIGSITES")] 
    [SerializeField] private Sprite _crossSprite;
    [SerializeField] private Sprite _pickAxeSprite;


    private Dictionary<string, CharacterType> _characterTypes;
    private Dictionary<string, PickAxeType> _pickAxeTypes;
    private Dictionary<string, TreasureType> _treasureTypes;
    private Dictionary<int, ZoneType> _zoneTypes;
    private Dictionary<int, DigSiteType> _digSiteTypes;
    
    public CharacterType[] Characters => _characters;

    public Sprite CrossSprite => _crossSprite;
    public Sprite PickAxeSprite => _pickAxeSprite;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        DontDestroyOnLoad(gameObject);
        
        _characterTypes = new Dictionary<string, CharacterType>();
        foreach (var character in _characters)
            _characterTypes.Add(character.CharacterName, character);
        
        _pickAxeTypes = new Dictionary<string, PickAxeType>();
        foreach (var pickAxe in _pickAxes)
            _pickAxeTypes.Add(pickAxe.name, pickAxe);
        
        _treasureTypes = new Dictionary<string, TreasureType>();
        foreach (var treasure in _treasures)
            _treasureTypes.Add(treasure.TreasureName, treasure);
        
        _zoneTypes = new Dictionary<int, ZoneType>();
        foreach (var zone in _zones)
            _zoneTypes.Add(zone.ZoneIndex, zone);
    }

    public CharacterType GetCharacterType(string cName)
    {
        return _characterTypes[cName];
    }
    
    public PickAxeType GetPickAxeType(string pName)
    {
        return _pickAxeTypes[pName];
    }
    
    public TreasureType GetTreasureType(string tName)
    {
        return _treasureTypes[tName];
    }

    public DigSiteType GetDigSiteType(int zoneIndex, int caveIndex)
    {
        var zone = _zoneTypes[zoneIndex];
        foreach (var digSite in zone.DigSites)
        {
            if (digSite.CaveIndex == caveIndex)
                return digSite;
        }

        return null;
    }
}
