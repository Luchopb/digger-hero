﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] private CharacterBehaviour[] _availableCharacters;
    [SerializeField] private TextMeshProUGUI _prestigeText;
    [Header("Resize Setup")]
    [SerializeField] private CameraResizer _cameraResizer;
    [SerializeField] private MainMenuManager _menuManager;

    private int _actualGold;
    private int _maxGold;
    private int _prestigeLevel = 1;
    private Dictionary<string, TreasureCollectionData> _obtainedTreasures;

    public int ActualGold => _actualGold;
    public int MaxGold => _maxGold;
    public CharacterBehaviour[] AvailableCharacters => _availableCharacters;

    private void Awake()
    {
        if(Instance != null) Destroy(this);
        
        Instance = this;
        _obtainedTreasures = new Dictionary<string, TreasureCollectionData>();
    }

    private void Start()
    {
        InitialScreenSetup();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            Time.timeScale = 1f;
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
            Time.timeScale = 2f;
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
            Time.timeScale = 3f;
        
        if (Input.GetKeyDown(KeyCode.Alpha4))
            Time.timeScale = 4f;
        
        if (Input.GetKeyDown(KeyCode.Alpha5))
            Time.timeScale = 5f;

        if (Input.GetKeyDown(KeyCode.T))
        {
            foreach (var treasureCollectionData in _obtainedTreasures)
            {
                for (int i = 0; i < treasureCollectionData.Value.LevelAmount.Count; i++)
                {
                    print(treasureCollectionData.Key + " tengo " + treasureCollectionData.Value.LevelAmount[i].Amount + " de level " + treasureCollectionData.Value.LevelAmount[i].Level);
                }
            }
        }
    }

    public void ObtainTreasure(TreasureType treasure, int level = 1)
    {
        if (_obtainedTreasures.ContainsKey(treasure.TreasureName))
            _obtainedTreasures[treasure.TreasureName].AddTreasure(level);
        else
        {
            _obtainedTreasures.Add(treasure.TreasureName, new TreasureCollectionData(treasure));

            MuseumManager.Instance.ObtainTreasure(treasure.TreasureName);
        }
    }

    public void UpgradeTreasure(TreasureType treasure, int level)
    {
        var t = _obtainedTreasures[treasure.TreasureName];

        t?.Upgrade(level);
    }

    private void InitialScreenSetup()
    {
        //Acomodo la camara segun la pantalla
        _cameraResizer.ResizeScreen();
        //Una vez acomodada la camara, ubico las distintas pantallas
        _menuManager.SetScreens();
    }

    public void AddPrestige(int amount)
    {
        _prestigeLevel += amount;
        _prestigeText.text = _prestigeLevel.ToString();
    }

    public CharacterBehaviour SendCharacterToDig(CharacterDisplay character, DigSiteType digSite)
    {
        //Encuentra al primer personaje disponible
        for (int i = 0; i < _availableCharacters.Length; i++)
        {
            if (!_availableCharacters[i].Digging)
            {
                //Setea al personaje activo
                _availableCharacters[i].ActivateCharacter(digSite, character.CharacterType);
                character.SetDigger(_availableCharacters[i]);
                DigSceneManager.Instance.ChangeDigScene(digSite, _availableCharacters[i]);
                return _availableCharacters[i];
            }
        }

        return null;
    }

    public void ChargeGameData(GameSaveData gameData, bool firstLoad, int timeDifference)
    {
        
        //Regenero lo del juego cuando lo abris
        if (firstLoad)
        {
            //Cargo personajes activos
            for (int i = 0; i < gameData.CharacterData.Length; i++)
            {
                if (!gameData.CharacterData[i].Digging /*|| gameData.CharacterData[i].CharacterIndexMenu < 0*/) continue;
            
                var character = gameData.CharacterData[i];
                _availableCharacters[character.CharacterIndexMenu].ResumeDigging(DataBaseManager.Instance.GetDigSiteType(character.ActiveSite.ZoneIndex, character.ActiveSite.CaveIndex),
                    DataBaseManager.Instance.GetCharacterType(character.CharacterName), character.ActiveSite.PlayerDepth, timeDifference);

                var characterToOccupy = _availableCharacters[character.CharacterIndexMenu];
                CharactersManager.Instance.OccupyCharacter(characterToOccupy, character.CharacterName);
                ZoneManager.Instance.OccupyDigSite(character.ActiveSite.ZoneIndex, character.ActiveSite.CaveIndex, characterToOccupy);
            }

            _prestigeLevel = gameData.PrestigeLevel;
            _prestigeText.text = _prestigeLevel.ToString();
            
            MuseumManager.Instance.LoadMuseumData(gameData.MuseumData);
        }//Calculo unicamente el oro generado y lo que excavaron en el tiempo transcurrido
        else
        {
            
        }
    }

    public GameSaveData GenerateGameSaveData()
    {
        //TODO: generar nuevo game save data para primer arranque del juego

        var cData = CharactersManager.Instance.GenerateCharactersData();
        var mData = MuseumManager.Instance.GenerateMuseumSaveData();
        var digData = ZoneManager.Instance.GenerateDigZoneSaveData();
        
        var gData = new GameSaveData(cData, null, null, mData, null, digData, _actualGold, _maxGold, _prestigeLevel);
        
        return gData;
    }
}
