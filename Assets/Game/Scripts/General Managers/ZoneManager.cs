﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ZoneManager : MenuScreen
{
    public static ZoneManager Instance { get; private set; }

    [SerializeField] private Transform _mapContainer;
    [SerializeField] private float _minDragDistance;
    [SerializeField] private Material _outlineMaterial;
    [SerializeField] private Material _normalMaterial;
    [SerializeField] private ZoneController[] _zones;
    [Header("UI SETTINGS")]
    [SerializeField] private Transform _treasureSitePositioner;
    [SerializeField] private Transform _treasureSiteContainer;
    [SerializeField] private TextMeshProUGUI _zoneTitleText;
    [SerializeField] private Button _closeDigMenuButton;
    [SerializeField] private Button _chooseDigSiteButton;
    [SerializeField] private TextMeshProUGUI _titleText;
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private Image[] _possibleTreasures;

    private int _zoneIndex;
    private bool _changingZone;
    private ZoneController _activeZone;
    
    private bool _dragging;
    private Camera _mainCamera;
    private Vector3 _initialDragPosition;
    private float _touchDistance;
    
    private MapDigSite _selectedDigSite;
    private Vector3 _treasureContainerOriginalPosition;

    public bool ChangingZone => _changingZone;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        
        _closeDigMenuButton.onClick.AddListener(HideDigSiteInfo);
        _chooseDigSiteButton.onClick.AddListener(ChooseDigSite);
    }

    protected override void Start()
    {
        base.Start();
        _treasureContainerOriginalPosition = _treasureSiteContainer.localPosition;
        _activeZone = _zones[_zoneIndex];
        
        foreach (var t in _possibleTreasures)
            t.gameObject.SetActive(false);

        SwipeDetectionManager.OnSwipe += ChangeZoneIndex;
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        /*
        if (!Active) return;
#if  !UNITY_EDITOR && UNITY_ANDROID
        if (Input.touches.Length > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                _initialDragPosition = Input.GetTouch(0).position;
            }
            if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (_dragging)
                    DragContainer(Input.GetTouch(0).position);
                else
                    _dragging = Mathf.Abs(_initialDragPosition.y - Input.GetTouch(0).position.y) > _minDragDistance;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                ReleaseContainer();
            }
        }
#elif UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            _initialDragPosition = Input.mousePosition;
        }
        
        if (Input.GetMouseButton(0))
        {
            if(_dragging)
                DragContainer(Input.mousePosition);
            else
                _dragging = Mathf.Abs(_initialDragPosition.y - Input.mousePosition.y) > _minDragDistance;
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            ReleaseContainer();
        }
#endif
*/
    }
    
    private void DragContainer(Vector3 mousePosition)
    {
        //La distancia en mundo del mouse inicial al objeto a mover
        var initialWorldPosition = _mainCamera.ScreenToWorldPoint(_initialDragPosition);
        _touchDistance = _mainCamera.ScreenToWorldPoint(mousePosition).y - initialWorldPosition.y;
        
        if ((_zoneIndex <= 0 && _touchDistance > 0) ||
            (_zoneIndex >= _zones.Length - 1 && _touchDistance < 0)) return;

        for (int i = 0; i < _zones.Length; i++)
        {
            _zones[i].transform.position =
                new Vector3(_zones[i].transform.position.x, _zones[i].transform.position.y + _touchDistance,0); //Camera.main.ScreenToWorldPoint(distance).x;
        }

        _initialDragPosition = mousePosition;
    }
    
    private void ReleaseContainer()
    {
        int newIndex = 0;
        var closestDistance = Vector2.Distance(_mainCamera.transform.position, _zones[0].transform.position);
        for (int i = 1; i < _zones.Length; i++)
        {
            var newDistance = Vector2.Distance(_mainCamera.transform.position, _zones[i].transform.position);
            if (newDistance < closestDistance)
            {
                newIndex = i;
                closestDistance = newDistance;
            }
        }
        
        _dragging = false;
        var goingDown = _zoneIndex < newIndex;
        _zoneIndex = newIndex;
        ChangeZone(goingDown);
    }
    
    public void ShowDigSiteInfo(MapDigSite dType, float yPosition)
    {
        if (_changingZone) return;
        
        if(DOTween.IsTweening(_treasureSiteContainer))
            DOTween.Kill(_treasureSiteContainer);
        
        if(_selectedDigSite != null)
            _selectedDigSite.MySprite.material = _normalMaterial;//.SetInt(_showOutline, 0);
        
        _treasureSiteContainer.DOLocalMoveY(_treasureSitePositioner.localPosition.y, 0.75f);
        _selectedDigSite = dType;
        _selectedDigSite.MySprite.material = _outlineMaterial;//.SetInt(_showOutline, 1);

        _chooseDigSiteButton.interactable = !dType.Occupied;
        
        _mapContainer.DOLocalMoveY(Camera.main.transform.position.y - yPosition, 1f);
        
        var minutes = Mathf.FloorToInt(_selectedDigSite.MyDigSiteType.TimeToReach / 60);
        var seconds = Mathf.FloorToInt(_selectedDigSite.MyDigSiteType.TimeToReach - minutes * 60);
        _timeText.text = string.Format("{0:0}:{1:00}", minutes, seconds);

        _titleText.text = "Dig " + (_selectedDigSite.MyDigSiteType.ZoneIndex + 1) + " - " + (_selectedDigSite.MyDigSiteType.CaveIndex + 1);
        
        
        foreach (var t in _possibleTreasures)
            t.gameObject.SetActive(false);
        
        for (var i = 0; i < _selectedDigSite.MyDigSiteType.PossibleTreasures.Length; i++)
        {
            _possibleTreasures[i].sprite = _selectedDigSite.MyDigSiteType.PossibleTreasures[i].Treasure.TreasureSprite;
            _possibleTreasures[i].gameObject.SetActive(true);
        }
    }

    private void ChangeZoneIndex(SwipeData swipeData)
    {
        if(!Active) return;
        
        if (swipeData.Direction == SwipeDirection.Up)
        {
            _zoneIndex++;
            if (_zoneIndex >= _zones.Length)
                _zoneIndex = 0;
            
            ChangeZone(false);
        }

        if (swipeData.Direction == SwipeDirection.Down)
        {
            _zoneIndex--;
            if (_zoneIndex < 0)
                _zoneIndex = _zones.Length - 1;
                
            ChangeZone(true);
        }
    }

    private void ChangeZone(bool goingDown)
    {
        _changingZone = true;
        HideDigSiteInfo();
        var direction = goingDown ? -1 : 1;

        var auxZone = _activeZone;
        _activeZone = _zones[_zoneIndex];
        
        if (DOTween.IsTweening(auxZone.transform))
        {
            DOTween.Kill(_activeZone.transform);
            DOTween.Kill(auxZone.transform);
        }
        
        auxZone.transform.DOLocalMoveY(20 * direction, .75f).OnComplete(() => auxZone.Moving = false);
        _activeZone.Moving = true;
        _activeZone.transform.localPosition = new Vector3(0, 20 * -direction);
        _activeZone.transform.DOLocalMoveY(0, .75f).OnComplete(() =>
        {
            _changingZone = false;
            _activeZone.Moving = false;
        });
        
        _zoneTitleText.DOFade(0, 0.25f).OnComplete(() =>
        {
            _zoneTitleText.text = _activeZone.ZoneType.ZoneName;
            _zoneTitleText.DOFade(1, 0.5f);
        });
    }

    public void FinishDigSite(int zoneIndex, int caveIndex)
    {
        if (_zones.Length < zoneIndex ||
            _zones[zoneIndex].MapMapDigSites.Length < caveIndex) 
            return;
        
        _zones[zoneIndex].FinishDigSite(caveIndex);
        SaveDataManager.Instance.UnlockDigSite(zoneIndex, caveIndex);
    }
    
    private void ChooseDigSite()
    {
        if (_changingZone || _selectedDigSite == null) return;
        
        var character = GameManager.Instance.SendCharacterToDig(CharactersManager.Instance.GetAvailableCharacter(), 
            _selectedDigSite.MyDigSiteType);

        if (character == null) return;
        
        _selectedDigSite.OccupyDigSite(character);
        HideDigSiteInfo();
    }

    public void OccupyDigSite(int zoneIndex, int caveIndex, CharacterBehaviour character)
    {
        if (_zones.Length < zoneIndex ||
            _zones[zoneIndex].MapMapDigSites.Length < caveIndex
            || _zones[zoneIndex].MapMapDigSites[caveIndex].Discovered) 
            return;
        
        _zones[zoneIndex].OccupyDigSite(caveIndex, character);
    }
    
    public void HideDigSiteInfo()
    {
        if(DOTween.IsTweening(_treasureSiteContainer))
            DOTween.Kill(_treasureSiteContainer);

        if (_selectedDigSite != null)
            _selectedDigSite.MySprite.material = _normalMaterial;//.SetInt(_showOutline, 0);
        
        _mapContainer.DOLocalMoveY(0, 1f);
        _selectedDigSite = null;
        _treasureSiteContainer.DOLocalMoveY(_treasureContainerOriginalPosition.y, 0.75f).SetEase(Ease.OutQuad);
    }

    public DigZoneSaveData[] GenerateDigZoneSaveData()
    {
        var digData = new DigZoneSaveData[_zones.Length];
        
        for (int i = 0; i < _zones.Length; i++)
        {
            var actualZone = _zones[i];
            digData[i] = new DigZoneSaveData(actualZone.ZoneType.ZoneName, actualZone.GenerateDigSitesData(), actualZone.Unlocked);
        }
        
        return digData;
    }
}
