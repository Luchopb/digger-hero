﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetectionManager : MonoBehaviour
{
    [SerializeField] private bool _detectSwipeAfterRelease;
    [SerializeField] private float _minSwipeDistance = 80f;
    [SerializeField] private float _minSwipeSpeed;

    private Vector2 _fingerDownPosition;
    private Vector2 _fingerUpPosition;
    private Touch _activeTouch;

    private float _initialTime;

    public static event Action<SwipeData> OnSwipe = delegate {  };

    private void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        _minSwipeSpeed *= 2.5f;
#endif
    }

    private void Update()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if (Input.touches.Length > 0)
        {
            var touch = Input.GetTouch(0);
            
            if (touch.phase == TouchPhase.Began)
            {
                _fingerUpPosition = touch.position;
                _fingerDownPosition = touch.position;
                _activeTouch = touch;
                _initialTime = Time.time;
            }

            if (!_detectSwipeAfterRelease && touch.phase == TouchPhase.Moved)
            {
                _fingerDownPosition = touch.position;
                DetectSwipe();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                _fingerDownPosition = touch.position;
                DetectSwipe();
            }
        }
#elif UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {  
            _fingerUpPosition = Input.mousePosition;
            _fingerDownPosition = Input.mousePosition;
            _initialTime = 0;
            _initialTime = Time.time;
        }

        
        if (!_detectSwipeAfterRelease && Input.GetMouseButton(0))
        {
            _fingerDownPosition = Input.mousePosition;
            DetectSwipe();
        }
        
        if (Input.GetMouseButtonUp(0))
        {  
            _fingerDownPosition = Input.mousePosition;
            DetectSwipe();
        }
#endif
    }

    private void DetectSwipe()
    {
        var elapsedTime = Time.time - _initialTime;
        var speed = Vector2.Distance(_fingerUpPosition, _fingerDownPosition) / elapsedTime;
        if (speed >= _minSwipeSpeed && (Mathf.Abs(_fingerDownPosition.y - _fingerUpPosition.y) > _minSwipeDistance ||
            Mathf.Abs(_fingerDownPosition.x - _fingerUpPosition.x) > _minSwipeDistance))
        {
            if (Mathf.Abs(_fingerDownPosition.y - _fingerUpPosition.y) >
                Mathf.Abs(_fingerDownPosition.x - _fingerUpPosition.x))
            {
                var direction = _fingerDownPosition.y - _fingerUpPosition.y > 0
                    ? SwipeDirection.Up
                    : SwipeDirection.Down;
                SendSwipe(direction);
            }
            else
            {
                var direction = _fingerDownPosition.x - _fingerUpPosition.x > 0
                    ? SwipeDirection.Right
                    : SwipeDirection.Left;
                SendSwipe(direction);
            }

            _fingerUpPosition = _fingerDownPosition;
        }
        else
            SendSwipe(SwipeDirection.None);
    }

    public void SendSwipe(SwipeDirection dir)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = dir,
            StartPosition = _fingerDownPosition,
            EndPosition = _fingerUpPosition
        };
        OnSwipe.Invoke(swipeData);
    }
}

public struct SwipeData
{
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection Direction;
}

public enum SwipeDirection{ Up, Down, Left, Right, None}
