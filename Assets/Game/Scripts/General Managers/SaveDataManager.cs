﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataManager : MonoBehaviour
{
    public static SaveDataManager Instance { get; private set; }

    private GameSaveData _gameSaveData;
    private string _time;

    public GameSaveData GameSaveData => _gameSaveData;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        _gameSaveData = SaveAndLoadSystem.Instance.LoadGameData();
        
        if(_gameSaveData != null)
            ResumeGame();
        else
            _gameSaveData = GameManager.Instance.GenerateGameSaveData();
    }

    //Al pausar el juego, guardo el tiempo
    //Al resumir, hago un soft load
    private void OnApplicationPause(bool pauseStatus)
    {
        if(!pauseStatus)
            ResumeGame(false);
        else
            SetTimeStamp();
    }
    
    //Al cerrar, guardo el tiempo y creo el archivo
    private void OnApplicationQuit()
    {
        SaveGameOnQuit();
    }
    
    private void SetTimeStamp()
    {
        _time = DateTime.Now.ToBinary().ToString();
        PlayerPrefs.SetString("time", _time);
    }
    
    //Cargo los datos. distinguiendo entre hard y soft cargado
    private void ResumeGame(bool firstLoad = true)
    {
        _time = PlayerPrefs.GetString("time");
            
        if (_gameSaveData != null && !string.IsNullOrEmpty(_time))
        {
            //Paso el string del momento en que se guardo a datetime
            var savedTime = DateTime.FromBinary(Convert.ToInt64(_time));
            //Lo comparo con le tiempo actual
            TimeSpan diff = DateTime.Now.Subtract(savedTime);
            GameManager.Instance.ChargeGameData(_gameSaveData, firstLoad, (int)Math.Round(diff.TotalSeconds));
        }
    }

    public DigSiteSaveData[] GetZoneDigSites(string zoneName)
    {
        if (_gameSaveData == null) return null;
        
        foreach (var digZoneSaveData in _gameSaveData.DigZonesData)
        {
            if (digZoneSaveData.ZoneName == zoneName)
                return digZoneSaveData.DigSites;
        }

        return null;
    }

    public void UnlockDigSite(int zoneIndex, int caveIndex)
    {
        _gameSaveData.DigZonesData[zoneIndex].DigSites[caveIndex].Unlocked = true;
        SaveFileData();
    }

    public void UpdateMuseumPrestige(int newPrestige)
    {
        _gameSaveData.MuseumData.MuseumLevel = newPrestige;
        SaveFileData();
    }

    public void UpdateCharacterPickAxeLevel(string cName, string pName, int newLevel)
    {
        foreach (var pickAxe in _gameSaveData.ObtainedPickAxes)
        {
            if (pickAxe.MaterialName != pName) continue;
            
            pickAxe.PickAxeLevel = newLevel;
            break;
        }
        SaveFileData();
    }

    public void UpdateCharacterStatus(string cName, CharacterBehaviour character)
    {
        foreach (var characterSaveData in _gameSaveData.CharacterData)
        {
            if (characterSaveData.CharacterName == cName)
            {
                characterSaveData.Digging = character.Digging;
                if (character.Digging)
                {
                    characterSaveData.CharacterIndexMenu = character.MenuIndex;
                    characterSaveData.ActiveSite = new ActiveTreasureSiteSaveData(character.ActiveDigSite.ZoneIndex,
                        character.ActiveDigSite.CaveIndex, character.Depth, character.ActiveDigSite.TimeToReach);
                }
                else
                {
                    characterSaveData.CharacterIndexMenu = -1;
                    characterSaveData.ActiveSite = null;
                }
            }
        }
        //TODO: pensar si rinde guardar tan seguido el archivo
        //SaveFileData();
    }
    
    public void UpdatePedestalLevel(int pIndex, int newLevel)
    {
        foreach (var pedestal in _gameSaveData.MuseumData.Pedestals)
        {
            if (pedestal.Index != pIndex) continue;
            
            pedestal.Level = newLevel;
            break;
        }
        SaveFileData();
    }
    
    public void ObtainNewTreasure(string tName)
    {
        foreach (var pedestal in _gameSaveData.MuseumData.Pedestals)
        {
            if (pedestal.TreasureName != tName) continue;
            
            pedestal.TreasureObtained = true;
            //pedestal.
            break;
        }
        SaveFileData();
    }
    
    public void SaveGameOnQuit()
    {
        SetTimeStamp();
        //TODO: que actualice el oro, el tiempo y la profundidad de cada pj (las cosas que cambian constantemente)

        if (_gameSaveData != null)
        {
            _gameSaveData.ObtainedGold = GameManager.Instance.ActualGold;
            _gameSaveData.MaxGold = GameManager.Instance.MaxGold;

            //TODO: una vez que esten los pjs, tiene que tomar la info de ellos y generar los active digsitedata
            /*foreach (var character in GameManager.Instance.AvailableCharacters)
            {
                if (!character.Digging) continue;

                for (var i = 0; i < _gameSaveData.CharacterData.Length; i++)
                {
                    CharacterSaveData characterSaveData = _gameSaveData.CharacterData[i];
                    if (characterSaveData.CharacterName == character.MyCharacter.CharacterName)
                    {
                        characterSaveData.Digging = true;
                        characterSaveData.CharacterIndexMenu = character.MenuIndex;
                        characterSaveData.ActiveSite = new ActiveTreasureSiteSaveData(character.ActiveDigSite.ZoneIndex,
                            character.ActiveDigSite.CaveIndex, character.Depth, character.ActiveDigSite.TimeToReach);
                        //ActiveTreasureSiteSaveData playerSite = new ActiveTreasureSiteSaveData();
                    }
                }
            }*/
        }

        SaveFileData();
    }
    
    private void SaveFileData()
    {
        SaveAndLoadSystem.Instance.SaveGameData(_gameSaveData);
    }
}
