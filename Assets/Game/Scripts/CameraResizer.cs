﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

public class CameraResizer : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _mainBackground;
    [SerializeField] private Camera _mainCamera;

    public void ResizeScreen()
    {
        var size = _mainBackground.bounds.size.x * Screen.height / Screen.width / 2;
        _mainCamera.orthographicSize = size;
    }
}
