﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character")]
public class CharacterType : ScriptableObject
{
    [SerializeField] private string _characterName;
    [SerializeField] private float _digLength = 1f;
    [TextArea]
    [SerializeField] private string _characterDescription;
    [Space(10)] 
    [SerializeField] private Sprite _characterUiSprite;
    [SerializeField] private RuntimeAnimatorController _characterAnimator;

    public string CharacterName => _characterName;
    public float DigLength => _digLength;
    public string CharacterDescription => _characterDescription;
    public Sprite CharacterUiSprite => _characterUiSprite;
    public RuntimeAnimatorController CharacterAnimator => _characterAnimator;
}
