﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Zone")]
public class ZoneType : ScriptableObject
{
    [SerializeField] private string _zoneName;
    [SerializeField] private int _zoneIndex;
    [SerializeField] private Sprite _zoneBackground;
    [SerializeField] private DigSiteType[] _digSites;
    
    public string ZoneName => _zoneName;
    public int ZoneIndex => _zoneIndex;
    public Sprite ZoneBackground => _zoneBackground;
    public DigSiteType[] DigSites => _digSites;
}
