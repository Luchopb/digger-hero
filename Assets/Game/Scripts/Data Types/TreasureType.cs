﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Treasure")]
public class TreasureType : ScriptableObject
{
    [SerializeField] private string _treasureName;
    [SerializeField] private Rarity _treasureRarity;
    [SerializeField] private Tier _treasureTier;
    [SerializeField] private int _goldPerTick;
    [SerializeField] private Sprite _treasureSprite;
    [TextArea]
    [SerializeField] private string _description;

    public string TreasureName => _treasureName;
    public Rarity TreasureRarity => _treasureRarity;
    public Tier TreasureTier => _treasureTier;
    public int GoldPerTick => _goldPerTick;
    public Sprite TreasureSprite => _treasureSprite;
    public string Description => _description;
}

public enum Rarity { Common, Uncommon, Rare, Epic, Legendary, Unique}
public enum Tier { None, Aztec, Asian, Egyptian, Medieval, Modern}

[System.Serializable]
public class TreasureChanceData
{
    [SerializeField] private TreasureType _treasure;
    [Range(0, 1)] [SerializeField] private float _chance;

    public TreasureType Treasure => _treasure;
    public float Chance => _chance;
}