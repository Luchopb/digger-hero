﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PickAxe")]
public class PickAxeType : ScriptableObject
{
    [Header("Cost Settings")] 
    [SerializeField] private PickAxeMaterial _pickPickAxeMaterial;
    [SerializeField] private float _initialMaterialCost;
    [SerializeField] private float _initialMoneyCost;
    [SerializeField] private float _costMultiplier;

    public float InitialMoneyCost => _initialMoneyCost;
    public float InitialMaterialCost => _initialMaterialCost;
    public float CostMultiplier => _costMultiplier;
    public PickAxeMaterial PickPickAxeMaterial => _pickPickAxeMaterial;
}

public enum PickAxeMaterial { Wood, Stone, Iron, Gold, Diamond, Obsidian}
