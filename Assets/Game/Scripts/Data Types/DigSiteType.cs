﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DigSite")]
public class DigSiteType : ScriptableObject
{
    [SerializeField] private int _zoneIndex;
    [SerializeField] private int _caveIndex;
    [SerializeField] private float _timeToReach;
    [SerializeField] private TreasureChanceData[] _possibleTreasures;

    public int ZoneIndex => _zoneIndex;
    public int CaveIndex => _caveIndex;
    public float TimeToReach => _timeToReach;
    public TreasureChanceData[] PossibleTreasures => _possibleTreasures;
}
