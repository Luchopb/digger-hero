﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TreasureCollectionData
{
    private TreasureType _myTreasureType;
    private List<LevelAmount> _levelAmount = new List<LevelAmount>();

    public TreasureType MyTreasureType => _myTreasureType;
    public List<LevelAmount> LevelAmount => _levelAmount;

    public TreasureCollectionData(TreasureType treasure)
    {
        _myTreasureType = treasure;
        AddTreasure(1);
    }

    public void AddTreasure(int level)
    {
        //Si ya existe un tesoro de ese nivel, aumenta la cantidad en uno
        for (int i = 0; i < _levelAmount.Count; i++)
        {
            if (_levelAmount[i].Level != level) continue;
            
            _levelAmount[i].Amount++;
            //SaveDataManager.Instance
            return;
        }
        //Si no, genera uno nuevo
        //TODO: que actualice el activo en el museo
        _levelAmount.Add(new LevelAmount(level, 1));
    }

    public void Upgrade(int level)
    {
        for (int i = 0; i < _levelAmount.Count; i++)
        {
            if (_levelAmount[i].Level != level) continue;

            //Si no tiene la cantidad necesaria, retorna (failsafe)
            if (_levelAmount[i].Amount < 3) return;
            
            _levelAmount[i].Amount -= 3;
            AddTreasure(level + 1);
        }
    }
}

[System.Serializable]
public class LevelAmount
{
    public int Level;
    public int Amount;
    
    public LevelAmount(int level, int amount)
    {
        Level = level;
        Amount = amount;
    }
}
