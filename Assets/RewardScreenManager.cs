﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class RewardScreenManager : MenuScreen
{
    public static RewardScreenManager Instance { get; private set; }

    [SerializeField] private SpriteRenderer _rewardSprite;
    [SerializeField] private TextMeshProUGUI _rewardTitle;
    [SerializeField] private TextMeshProUGUI _continueText;
    
    private Animator _rewardAnimation;
    private bool _sequenceFinished;

    private void Awake()
    {
        if(Instance != null) Destroy(this);

        Instance = this;
    }

    public void ShowTreasureReward(TreasureType treasure)
    {
        MainMenuManager.Instance.LeaveMenuScreen(this);
        MainMenuManager.Instance.TurnMainCanvasOff();
        
        _sequenceFinished = false;
        _continueText.text = "";
        _rewardSprite.sprite = treasure.TreasureSprite;
        _rewardTitle.text = "Obtuviste " + treasure.TreasureName;
        
        var treasureSequence = DOTween.Sequence();
        treasureSequence.OnComplete(EndRewardSequence);
        
        treasureSequence.Append(_rewardSprite.transform.DOScale(Vector3.one, .5f));
        treasureSequence.Append(_rewardSprite.DOColor(Color.white, .5f));
        treasureSequence.Append(_rewardTitle.transform.DOScale(Vector3.one, .25f));
    }

    private void EndRewardSequence()
    {
        _sequenceFinished = true;
        _continueText.text = "Tocar para continuar";
    }
    
    private void OnMouseDown()
    {
        if (!_sequenceFinished) return;
        
        _rewardSprite.transform.localScale = Vector3.zero;
        _rewardTitle.transform.localScale = Vector3.zero;
        _rewardSprite.color = Color.black;
        MainMenuManager.Instance.ReturnToMenuScreen(3);
    }
}
